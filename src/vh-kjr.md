# VH-KJR

### 20171214

* CG arm: `996mm`
* Basic Empty Weight `670.3kg, 1477.7lb`
* Moment: `667.334 kg/mm`
* Maximum Ramp Weight: `2400lb`
* Maximum Take-off Weight: `2400lb`
