# VH-LSE

### 20161130

* Weighted: `12/2/2002`
* Printed: `12/17/2002`
* Serial: `172S9282`
* Registration: `N53112`
* Weight (lbs): `1663.2`
* Arm (in): `40.719`
* Moment (lb-in): `67724`
* Basic Empty Weight
  * Weight (lbs): `1691.6`
  * Arm (in): `40.600`
  * Moment (lb-in): `68679`
* Useful load: `886.4`
* Maximum Ramp Weight: `2558.0`
* Maximum Take-off Weight: `2550.0`
